/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy.marshal;

import java.lang.reflect.Array;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.JSONMarshaler;
import cc.plural.jsonij.marshal.Inspector;
import cc.plural.jsonij.marshal.InspectorProperty;
import cc.plural.jsonij.marshal.annotation.JSONAccessor;
import cc.plural.jsonij.marshal.annotation.JSONIgnore;
import cc.plural.jsonij.marshal.annotation.JSONMutator;
import cc.plural.jsonij.marshal.annotation.JSONName;
import static org.junit.Assert.*;

/**
 *
 * @author openecho
 */
public class InspectorTest {

    @Test
    public void testGetObjectMethods() {
        TestClass1 test1 = new TestClass1();
        Inspector oi = new Inspector(test1);
        InspectorProperty[] methods = oi.getMethodProperties(test1.getClass());
        assertEquals(methods.length, 2);
    }

    @Test
    public void testGetObjectFields() {
        TestClass1 test1 = new TestClass1();
        Inspector oi = new Inspector(test1);
        InspectorProperty[] methods = oi.getAttributeProperties(test1.getClass());
        assertEquals(methods.length, 2);
    }

    @Test
    public void testGetInspectorProperties() {
        TestClass1 test1 = new TestClass1();
        Inspector oi = new Inspector(test1);
        oi.inspect();
        InspectorProperty[] props = oi.getProperties();
        assertEquals(props.length, 4);
    }

    @Test
    public void testObjectInspection() {
        System.out.println("object inspection test1");
        TestClass1 test1 = new TestClass1();
        Inspector oi = new Inspector(test1);
        oi.inspect();
        InspectorProperty[] props = oi.getProperties();
        for (int i = 0; i < Array.getLength(props); i++) {
            System.out.println(props[i].toString());
        }
        JSON test1JSON = JSONMarshaler.marshalObject(test1);
        System.out.println(test1JSON);

        System.out.println("object inspection test2");
        TestClass2 test2 = new TestClass2();
        Inspector oi2 = new Inspector(test2);
        oi2.inspect();
        InspectorProperty[] props2 = oi2.getProperties();
        for (int i = 0; i < Array.getLength(props2); i++) {
            System.out.println(props2[i].toString());
        }
        JSON test2JSON = JSONMarshaler.marshalObject(test2);
        System.out.println(test2JSON);
    }

    
    public class TestClass2 extends TestClass1 {

        // Valid Attributes attributeThree, attributeFour, attributeFive, attributeSix, blah2, blah

        public short blah = 11;

        public String getBlah2() {
            return "1111";
        }
    }

    public class TestClass1 {

        // Valid Attributes attributeThree, attributeFour, attributeFive, attributeSix

        @JSONIgnore
        public String attributeOne;
        private double attributeTwo;
        @JSONName("attributeThree")
        public long someOtherName;
        protected short thisNameIsImportantForSomeOtherReason;
        public boolean attributeFive;
        private boolean attributeSix;

        @JSONIgnore
        public double getAttributeTwo() {
            return attributeTwo;
        }

        @JSONIgnore
        public void setAttributeTwo(double attributeTwo) {
            this.attributeTwo = attributeTwo;
        }

        @JSONAccessor("attributeFour")
        public short ICalledTHisASiLLLLLLYName() {
            return thisNameIsImportantForSomeOtherReason;
        }

        @JSONMutator("attributeFour")
        public void zALGOIsTOnyThePony(short thisNameIsImportantForSomeOtherReason) {
            this.thisNameIsImportantForSomeOtherReason = thisNameIsImportantForSomeOtherReason;
        }

        public boolean isAttributeSix() {
            return attributeSix;
        }

        public void setAttributeSix(boolean attributeSix) {
            this.attributeSix = attributeSix;
        }
    }
}
