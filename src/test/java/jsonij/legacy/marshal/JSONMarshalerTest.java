/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy.marshal;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.JSONMarshaler;
import cc.plural.jsonij.marshal.JavaObjectMarshaler;
import cc.plural.jsonij.marshal.annotation.JSONAccessor;
import cc.plural.jsonij.marshal.annotation.JSONIgnore;
import cc.plural.jsonij.marshal.annotation.JSONMutator;
import cc.plural.jsonij.marshal.annotation.JSONName;
import cc.plural.jsonij.marshal.codec.DateJSONCodec;
import static org.junit.Assert.*;

/**
 *
 * @author openecho
 */
public class JSONMarshalerTest {

    /**
     * Test of marshalObject method, of class JSONMarshaler.
     */
    @Test
    public void testMarshalObject_Object() {
        System.out.println("marshalObject(Object)");
        ArrayList<Object> interests = new ArrayList<Object>();
        interests.add("Lego");
        interests.add("Programming");
        interests.add(null);
        interests.add(new Integer(1337));
        JSON result = JSONMarshaler.marshalObject(interests);
        System.out.println(result.getRoot().toJSON());

        Person o = new Person("John", "Marsden", new BirthData(8, 5, 1981), interests);
        result = JSONMarshaler.marshalObject(o);
        System.out.println(result.getRoot().toJSON());
    }

    @Test
    public void testMarshalObject_charArray() {
        System.out.println("marshalObject(char[])");
        char[] testArray = new char[]{'J', 'o', 'h', 'n'};
        JSON result = JSONMarshaler.marshalObject(testArray);
        System.out.println(result.getRoot().toJSON());
        assertEquals(result.getRoot().toJSON(), "[\"J\",\"o\",\"h\",\"n\"]");
    }

    @Test
    public void testMarshalObject_DoubleArray() {
        System.out.println("marshalObject(double[])");
        double[] testArray = new double[]{1D, 0D, -100D, 33D, 100000000D};
        JSON result = JSONMarshaler.marshalObject(testArray);
        System.out.println(result.getRoot().toJSON());
        assertEquals(result.getRoot().toJSON(), "[1.0,0.0,-100.0,33.0,1.0E8]");
    }

    @Test
    public void testMarshalObject_doubleArray() {
        System.out.println("marshalObject(Double[])");
        Double[] testArray = new Double[]{-1.1D, 0D, -100D, -33D, 10D};
        JSON result = JSONMarshaler.marshalObject(testArray);
        System.out.println(result.getRoot().toJSON());
        assertEquals(result.getRoot().toJSON(), "[-1.1,0.0,-100.0,-33.0,10.0]");
    }

    @Test
    public void testMarshalObject_List() {
        System.out.println("marshalObject(List)");
        List<String> list = new ArrayList<String>();
        list.add("Element1");
        list.add("Element2");
        JSON result = JSONMarshaler.marshalObject(list);
        System.out.println(result.getRoot().toJSON());
    }

    @Test
    public void testMarshalObject_Map() {
        System.out.println("marshalObject(Map)");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("Rah", new double[]{-1.1, 0.0, -100.0, -33.0, 10.0});
        map.put("test", null);
        map.put("Object", new BirthData(8, 5, 1981));
        JSON result = JSONMarshaler.marshalObject(map);
        System.out.println(result.getRoot().toJSON());
        //assertEquals(result.getRoot().toJSON(), "[-1.1, 0.0, -100.0, -33.0, 10.0]");
    }

    @Test
    public void testBookExample() {
        System.out.println("bookExample");
        List<Book> bookList = new ArrayList<Book>();
        bookList.add(new Book("John Marsden", "Tomorrow When The War Began"));
        bookList.add(new Book("David Johnsonbaugh", "Discrete Mathematics"));
        JSON output = JSONMarshaler.marshalObject(bookList);
        System.out.println(output.getRoot().toJSON());
    }

    @Test
    public void testDateExample() {
        System.out.println("date");
        Date[] dateTests = new Date[]{
            new Date()
        };
        JSON output = JSONMarshaler.marshalObject(dateTests);
        System.out.println("Non Codec Marshal:");
        System.out.println(output.toJSON());

        JavaObjectMarshaler.registerCodec(DateJSONCodec.class);

        System.out.println("Codec Marshal:");
        output = JSONMarshaler.marshalObject(dateTests);
        System.out.println(output.toJSON());
    }

     @Test
    public void testMyDateExample() {
        System.out.println("my date");
        MyDate[] dateTests = new MyDate[]{
            new MyDate()
        };
        JavaObjectMarshaler.registerCodec(DateJSONCodec.class);
        JSON output = JSONMarshaler.marshalObject(dateTests);
        System.out.println("My Date Marshal:");
        System.out.println(output.toJSON());
    }


    public class MyArray<S extends Book> extends ArrayList<S> {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -3901390653061741883L;
        
    }

    public class MyDate extends Date {

		/**
		 * Serial UID
		 */
		private static final long serialVersionUID = -5597217523524878075L;

    }

    public class Person {

        public String firstName;
        @JSONName("surname")
        public String lastName;
        public BirthData bd;
        public ArrayList<Object> interests;
        @JSONIgnore
        public long blah;
        private long privateVariable;
        private boolean test;

        public Person(String firstName, String lastName, BirthData bd, ArrayList<Object> interests) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.bd = bd;
            this.interests = interests;
        }

        @JSONAccessor("privateVariable")
        public long getRah() {
            return privateVariable;
        }

        @JSONMutator("privateVariable")
        public void setMeh(long privateVariable) {
            this.privateVariable = privateVariable;
        }

        public BirthData getBd() {
            return bd;
        }

        public void setBd(BirthData bd) {
            this.bd = bd;
        }

        public long getBlah() {
            return blah;
        }

        public void setBlah(long blah) {
            this.blah = blah;
        }

        public ArrayList<Object> getInterests() {
            return interests;
        }

        public void setInterests(ArrayList<Object> interests) {
            this.interests = interests;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public boolean isTest() {
            return test;
        }

        public void setTest(boolean test) {
            this.test = test;
        }

        public String getNonExistantMethod() {
            return "lol";
        }

        public void setNonExistantMethod(String value) {
        }
    }

    public class BirthData {

        public int day;
        public int month;
        public int year;

        public BirthData(int day, int month, int year) {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public String toString() {
            return "meh";
        }
    }

    public class Book {

        String author;
        String title;

        public Book() {
            this(null, null);
        }

        public Book(String author, String title) {
            this.author = author;
            this.title = title;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
