/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jsonij.legacy;

import java.io.IOException;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.parser.ParserException;
import static org.junit.Assert.*;

/**
 *
 * @author jmarsden
 */
public class ParserEqualsTest {

    final String googleJSONExample = "{\r\n"
            + "  \"version\": \"1.0\",\r\n"
            + "  \"encoding\": \"UTF-8\",\r\n"
            + "  \"feed\": {\r\n"
            + "    \"xmlns\": \"http://www.w3.org/2005/Atom\",\r\n"
            + "    \"xmlns$openSearch\": \"http://a9.com/-/spec/opensearchrss/1.0/\",\r\n"
            + "    \"xmlns$gd\": \"http://schemas.google.com/g/2005\",\r\n"
            + "    \"xmlns$gCal\": \"http://schemas.google.com/gCal/2005\",\r\n"
            + "    \"id\": {\"$t\": \"...\"},\r\n"
            + "    \"updated\": {\"$t\": \"2006-11-12T21:25:30.000Z\"},\r\n"
            + "    \"title\": {\r\n"
            + "      \"type\": \"text\",\r\n"
            + "      \"$t\": \"Google Developer Events\"\r\n"
            + "    },\r\n"
            + "    \"subtitle\": {\r\n"
            + "      \"type\": \"text\",\r\n"
            + "      \"$t\": \"The calendar contains information about upcoming developer conferences at which Google will be speaking, along with other developer-related events.\"\r\n"
            + "    },\r\n"
            + "    \"link\": [{\r\n"
            + "      \"rel\": \"...\",\r\n"
            + "      \"type\": \"application/atom+xml\",\r\n"
            + "      \"href\": \"...\"\r\n"
            + "      },{\r\n"
            + "      \"rel\": \"self\",\r\n"
            + "      \"type\": \"application/atom+xml\",\r\n"
            + "      \"href\": \"...\"\r\n"
            + "    }],\r\n"
            + "    \"author\": [{\r\n"
            + "      \"name\": {\"$t\": \"Google Developer Calendar\"},\r\n"
            + "      \"email\": {\"$t\": \"developer-calendar@google.com\"}\r\n"
            + "    }],\r\n"
            + "    \"generator\":{\r\n"
            + "      \"version\": \"1.0\",\r\n"
            + "      \"uri\": \"http://www.google.com/calendar\",\r\n"
            + "      \"$t\": \"Google Calendar\"\r\n"
            + "    },\r\n"
            + "    \"openSearch$startIndex\": {\"$t\": \"1\"},\r\n"
            + "    \"openSearch$itemsPerPage\": {\"$t\": \"25\"},\r\n"
            + "    \"gCal$timezone\": {\"value\": \"America/Los_Angeles\"},\r\n"
            + "\r\n"
            + "    \"entry\": [{\r\n"
            + "      \"id\": {\"$t\": \"...\"},\r\n"
            + "      \"published\": {\"$t\": \"2006-11-12T21:25:30.000Z\"},\r\n"
            + "      \"updated\": {\"$t\": \"2006-11-12T21:25:30.000Z\"},\r\n"
            + "      \"category\": [{\r\n"
            + "        \"scheme\": \"...\",\r\n"
            + "        \"term\": \"...\"\r\n"
            + "      }],\r\n"
            + "      \"title\":{\r\n"
            + "        \"type\": \"text\",\r\n"
            + "        \"$t\": \"WebmasterWorld PubCon 2006: Google Developer Tools in General\"\r\n"
            + "      },\r\n"
            + "      \"content\": {\r\n"
            + "        \"type\": \"text\",\r\n"
            + "        \"$t\": \"Google is sponsoring at <a href=\\\"http://www.pubcon.com/\\\">WebmasterWorld PubCon 2006</a>. \\nCome and visit us at the booth or join us for an evening demo reception where we will be talking \\\"5 ways to enhance your website with Google Code\\\".\\nAfter all,\\nit is Vegas, baby! See you soon.\"\r\n"
            + "      },\r\n"
            + "      \"link\": [{\r\n"
            + "        \"rel\": \"alternate\",\r\n"
            + "        \"type\": \"text/html\",\r\n"
            + "        \"href\": \"...\",\r\n"
            + "        \"title\": \"alternate\"\r\n"
            + "        },{\r\n"
            + "        \"rel\": \"self\",\r\n"
            + "        \"type\": \"application/atom+xml\",\r\n"
            + "	    \"href\": \"...\"\r\n"
            + "      }],\r\n"
            + "      \"author\": [{\r\n"
            + "        \"name\": {\"$t\": \"Google Developer Calendar\"},\r\n"
            + "        \"email\": {\"$t\": \"developer-calendar@google.com\"}\r\n"
            + "      }],\r\n"
            + "      \"gd$transparency\": {\"value\": \"http://schemas.google.com/g/2005#event.opaque\"},\r\n"
            + "      \"gd$eventStatus\": {\"value\": \"http://schemas.google.com/g/2005#event.confirmed\"},\r\n"
            + "      \"gd$comments\": {\"gd$feedLink\": {\"href\": \"...\"}},\r\n"
            + "      \"gCal$sendEventNotifications\": {\"value\": \"true\"},\r\n"
            + "      \"gd$when\": [{\r\n"
            + "        \"startTime\": \"2006-11-15\",\r\n"
            + "        \"endTime\": \"2006-11-17\",\r\n"
            + "        \"gd$reminder\": [{\"minutes\": \"10\"}]\r\n"
            + "      }],\r\n"
            + "      \"gd$where\": [{\"valueString\": \"3150 Paradise Road,Las Vegas,NV 89109\"}]}\r\n"
            + "     ]\r\n"
            + "  }\r\n"
            + "}";

    /**
     * Test of getStringReader method, of class JSONReader.
     */
    @Test
    public void testParseEquals() throws IOException, ParserException {
        System.out.println("testParseEquals");
        JSON parseJSON = JSON.parse(googleJSONExample);

        JSON resultParsed = JSON.parse(parseJSON.toJSON());
        assertEquals(parseJSON, resultParsed);

    }
}
