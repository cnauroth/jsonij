/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

package jsonij.legacy;


import java.io.IOException;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.parser.ParserException;
import static org.junit.Assert.*;


/**
 *
 * @author jmarsden
 */
public class PerformanceTests {



    @Test
    public void testUnicode() throws ParserException, IOException {
        System.out.println("Unicode");
        String testInput = "{\"lastname\":\"\\u44ff\",\"date\":\"\\u44aa\\u44ff\",\"len\":\"\\u44AA\"}";
        String testSoultion = "{\"lastname\":\"\\u44ff\",\"date\":\"\\u44aa\\u44ff\",\"len\":\"\\u44aa\"}";
        for(int i=0;i<500000;i++) {
            JSON json = JSON.parse(testInput);
            String testOutput = json.toJSON();
            assertEquals(testSoultion, testOutput);
        }
    }
}
