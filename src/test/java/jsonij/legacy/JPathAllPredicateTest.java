/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package jsonij.legacy;

import java.io.IOException;
import org.junit.Test;

import cc.plural.jsonij.JPath;
import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.jpath.Component;
import cc.plural.jsonij.parser.ParserException;
import static org.junit.Assert.*;

/**
 *
 * @author J.W.Marsden
 */
public class JPathAllPredicateTest {

    @Test
    public void testExpression1() throws IOException, ParserException {
        System.out.println("Test Expression1 - All Predicate JPath");
        JSON json = JSON.parse(JSONTestExamples.STORE_SAMPLE);
        JPath<Component> path = JPath.parse("/store/book[*]/author");
        Value[] values = path.evaluateAll(json);
        System.out.println("ResultSize:" + values.length);
        for(int i=0;i<values.length;i++) {
            System.out.println("[" + i + "]" + values[i].toJSON());
        }
        assertEquals(values.length, 4, 0);
        assertEquals(values[0],"Nigel Rees");
    }
}
