/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.plural.jsonij;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import static org.junit.Assert.*;

/**
 * JSON.String Type Tests
 * 
 * @author jmarsden@plural.cc
 */
public class JSONStringTest {

    public JSONStringTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConstruct() {
        System.out.println("construct");
        new JSON.String(null);
        new JSON.String("Value 2");
        new JSON.String("this is a string with a tab \t character");
        new JSON.String("this is a string with a %7BURL-encoded%7D character");
    }
    
    @Test
    public void testEquals() {
        System.out.println("equals");
        Value value1 = new JSON.String(null);
        assertEquals(value1, new JSON.String(""));
        
        Value value2 = new JSON.String("Value 2");
        assertEquals(value2, new JSON.String("Value 2"));
        
        Value value3 = new JSON.String("this is a string with a tab \t character");
        assertEquals(value3, new JSON.String("this is a string with a tab \t character"));
        
        Value value4 = new JSON.String("this is a string with a %7BURL-encoded%7D character");
        assertEquals(value4.toJSON(), "\"this is a string with a %7BURL-encoded%7D character\"");

        assertNotSame(value1, value2);
        assertNotSame(value2, value3);
        assertNotSame(value3, value4);
        
        
        assertEquals(value1, "");
        assertEquals(value2, "Value 2");
        assertEquals(value3, "this is a string with a tab \t character");
        assertEquals(value4, "this is a string with a %7BURL-encoded%7D character");
    }
    
    @Test
    public void testToJSON() {
        System.out.println("toJSON");
        Value value1 = new JSON.String(null);
        assertEquals(value1.toJSON(), "\"\"");
        Value value2 = new JSON.String("Value 2");
        assertEquals(value2.toJSON(), "\"Value 2\"");
        Value value3 = new JSON.String("this is a string with a tab \t character");
        assertEquals(value3.toJSON(), "\"this is a string with a tab \\t character\"");
        Value value4 = new JSON.String("this is a string with a %7BURL-encoded%7D character");
        assertEquals(value4.toJSON(), "\"this is a string with a %7BURL-encoded%7D character\"");
    }
    
    @Test
    public void testToString() {
        System.out.println("toString");
        Value value1 = new JSON.String(null);
        assertEquals(value1.toString(), "");
        Value value2 = new JSON.String("Value 2");
        assertEquals(value2.toString(), "Value 2");
        Value value3 = new JSON.String("this is a string with a tab \t character");
        assertEquals(value3.toString(), "this is a string with a tab \t character");
        Value value4 = new JSON.String("this is a string with a %7BURL-encoded%7D character");
        assertEquals(value4.toString(), "this is a string with a %7BURL-encoded%7D character");
    }
}