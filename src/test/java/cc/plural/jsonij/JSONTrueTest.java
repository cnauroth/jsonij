/*
 * Copyright 2011 jmarsden.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.plural.jsonij;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.Value;
import static org.junit.Assert.*;

/**
 * JSON.TRUE Type Tests
 * 
 * @author jmarsden@plural.cc
 */
public class JSONTrueTest {

    public JSONTrueTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConstruct() {
        System.out.println("construct");
        Value value = JSON.TRUE;
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        Value value = JSON.TRUE;
        assertEquals(value, JSON.TRUE);
    }

    @Test
    public void testToJSON() {
        System.out.println("toJSON");
        Value value = JSON.TRUE;
        assertEquals(value.toJSON(), "true");
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        Value value = JSON.TRUE;
        assertEquals(value.toJSON(), "true");
    }
}