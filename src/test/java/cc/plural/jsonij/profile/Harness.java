/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package cc.plural.jsonij.profile;

import java.io.IOException;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.parser.ParserException;

/**
 *
 * @author jmarsden
 */
public class Harness {

    public static void main(String[] args) throws ParserException, IOException {
                final String input = "   ["
                + "      {"
                + "         \"precision\": \"zip\","
                + "         \"Latitude\":  37.7668,"
                + "         \"Longitude\": -122.3959,"
                + "         \"Address\":   \"\","
                + "         \"City\":      \"SAN FRANCISCO\","
                + "         \"State\":     \"CA\","
                + "         \"Zip\":       \"94107\","
                + "         \"Country\":   \"US\""
                + "      },"
                + "      {"
                + "         \"precision\": \"zip\","
                + "         \"Latitude\":  37.371991,"
                + "         \"Longitude\": -122.026020,"
                + "         \"Address\":   \"\","
                + "         \"City\":      \"SUNNYVALE\","
                + "         \"State\":     \"CA\","
                + "         \"Zip\":       \"94085\","
                + "         \"Country\":   \"US\""
                + "      }"
                + "   ]";
        for (int i = 0; i < 10000; i++) {
            JSON json = JSON.parse(input);
            String outputJSON = json.toJSON();
        }
    }
}
