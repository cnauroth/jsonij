/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

package cc.plural.jsonij.jpath;

import java.util.ArrayList;
import java.util.List;

import cc.plural.jsonij.Value;

/**
 *
 * @author jmarsden
 */
public abstract class Component {
    
    public List<Value> evaluate(List<Value> values) {
        return evaluate(values, new ArrayList<Value>());
    }

    public abstract List<Value> evaluate(List<Value> values, List<Value> results);
}
