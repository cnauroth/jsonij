/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package cc.plural.jsonij;

import java.io.IOException;

import cc.plural.jsonij.marshal.JSONDocumentMarshaler;
import cc.plural.jsonij.marshal.JSONMarshalerException;
import cc.plural.jsonij.marshal.JavaMarshaler;
import cc.plural.jsonij.parser.ParserException;

/**
 *
 * @author openecho
 */
public class JSONMarshaler {

    protected static final JavaMarshaler javaMarshaler;
    protected static final JSONDocumentMarshaler jsonMarshaler;

    static {
        javaMarshaler = new JavaMarshaler();
        jsonMarshaler = new JSONDocumentMarshaler();
    }

    public static JSON marshalObject(Object o) {
        Value mashaledObject = javaMarshaler.marshalObject(o);
        return new JSON(mashaledObject);
    }

    public static JSON marshalObject(boolean[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Boolean[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(int[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Integer[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(char[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Character[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(double[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Double[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(float[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Float[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(short[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Short[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(long[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Long[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(String[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }

    public static JSON marshalObject(Object[] a) {
        Value marshaledArray = javaMarshaler.marshalObject(a);
        return new JSON(marshaledArray);
    }
    
    public static Object marshalJSON(String json, Class<?> c) throws JSONMarshalerException, IOException, ParserException {
        Object marshaledObject = jsonMarshaler.marshalJSONDocument(JSON.parse(json), c);
        return marshaledObject;
    }
    
    public static Object marshalJSON(JSON json, Class<?> c) throws JSONMarshalerException {
        Object marshaledObject = jsonMarshaler.marshalJSONDocument(json, c);
        return marshaledObject;
    }

}
