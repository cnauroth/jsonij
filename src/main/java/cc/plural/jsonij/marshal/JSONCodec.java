/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package cc.plural.jsonij.marshal;

import java.lang.reflect.Method;

import cc.plural.jsonij.Value;

/**
 *
 * @author openecho
 */
public abstract class JSONCodec<T extends Object> {

    int codecHash;

    Method encodeMethod;

    Method decodeMethod;

    int getCodecHash() {
        return codecHash;
    }

    void setCodecHash(int codecHash) {
        this.codecHash = codecHash;
    }

    Method getEncodeMethod() {
        return encodeMethod;
    }

    void setEncodeMethod(Method encodeMethod) {
        this.encodeMethod = encodeMethod;
    }

    Method getDecodeMethod() {
        return decodeMethod;
    }

    void setDecodeMethod(Method decodeMethod) {
        this.decodeMethod = decodeMethod;
    }
    
    public abstract JSONCodec<? extends Object> createInstance();

    public abstract Value encode(T o);

    public abstract T decode(Value value);
}