/*
 *  Copyright 2011 jmarsden.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package cc.plural.jsonij.marshal;

/**
 *
 * @author jmarsden
 */
public class InspectorProperty {

    public enum TYPE {
        FIELD, METHOD
    }
    public String propertyName;
    public String accessName;
    public String mutateName;
    public TYPE accessPropertyType;
    public TYPE mutatePropertyType;
    public Class<?> accessReturnType;
    public Class<?> mutateInputType;

    public InspectorProperty() {
        propertyName = null;
        accessPropertyType = null;
        mutatePropertyType = null;
        accessName = null;
        mutateName = null;
    }

    public InspectorProperty(String name, TYPE accessType) {
        this.propertyName = name;
        this.accessPropertyType = accessType;
        this.mutatePropertyType = accessType;
        accessName = null;
        mutateName = null;
    }

    public TYPE getAccessPropertyType() {
        return accessPropertyType;
    }

    public void setAccessPropertyType(TYPE propertyType) {
        this.accessPropertyType = propertyType;
    }

    public TYPE getMutatePropertyType() {
        return mutatePropertyType;
    }

    public void setMutatePropertyType(TYPE propertyType) {
        this.mutatePropertyType = propertyType;
    }

    public String getAccessName() {
        return accessName;
    }

    public void setAccessName(String accessName) {
        this.accessName = accessName;
    }

    public String getMutateName() {
        return mutateName;
    }

    public void setMutateName(String mutateName) {
        this.mutateName = mutateName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String name) {
        this.propertyName = name;
    }

    public Class<?> getAccessReturnType() {
        return accessReturnType;
    }

    public void setAccessReturnType(Class<?> accessReturnType) {
        this.accessReturnType = accessReturnType;
    }

    public Class<?> getMutateInputType() {
        return mutateInputType;
    }

    public void setMutateInputType(Class<?> mutateInputType) {
        this.mutateInputType = mutateInputType;
    }

    public boolean hasAccessor() {
        return accessPropertyType == TYPE.FIELD || ( accessName != null );
    }

    public boolean hasMutator() {
        return mutatePropertyType == TYPE.FIELD || ( mutateName != null );
    }

    @Override
    public String toString() {
        return String.format("Property %s (Access %s %s):(Mutate %s %s)", propertyName, accessReturnType, accessName, mutateInputType, mutateName);
    }
}
