/**
 * Copyright (C) 2010-2011 J.W.Marsden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package cc.plural.jsonij;

import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import static cc.plural.jsonij.Constants.CLOSE_OBJECT;
import static cc.plural.jsonij.Constants.NAME_SEPARATOR;
import static cc.plural.jsonij.Constants.OPEN_OBJECT;
import static cc.plural.jsonij.Constants.QUOTATION_MARK;

public class ObjectImp<CS extends CharSequence, V extends Value> extends Value implements java.util.Map<CS, V> {

    /**
	 * Serial UID
	 */
	private static final long serialVersionUID = 7601285884315492844L;
	
	/**
     * Holds the Mapping Values for the Object
     */
    protected LinkedHashMap<CS, V> mapValue;
    /**
     * Holds the Key order so values can be extracted in the order they were added.
     */
    protected ArrayList<CS> valueOrder;

    /**
     * Default Constructor
     */
    public ObjectImp() {
        mapValue = new LinkedHashMap<CS, V>();
        valueOrder = new ArrayList<CS>();
    }

    /* (non-Javadoc)
     * @see com.realitypipe.json.Value#internalType()
     */
    @Override
    protected TYPE internalType() {
        return TYPE.OBJECT;
    }

    /* (non-Javadoc)
     * @see java.util.Map#clear()
     */
    public void clear() {
        mapValue.clear();
    }

    /* (non-Javadoc)
     * @see java.util.Map#containsKey(java.lang.Object)
     */
    public boolean containsKey(Object key) {
        return mapValue.containsKey((CS) key);
    }

    /* (non-Javadoc)
     * @see java.util.Map#containsValue(java.lang.Object)
     */
    public boolean containsValue(Object value) {
        return mapValue.containsValue((V) value);
    }

    /* (non-Javadoc)
     * @see java.util.Map#entrySet()
     */
    public Set<java.util.Map.Entry<CS, V>> entrySet() {
        return mapValue.entrySet();
    }

    /* (non-Javadoc)
     * @see java.util.List#get(java.lang.Integer)
     */
    public V get(int i) {
        return mapValue.get((CS) valueOrder.get(i));
    }

    /* (non-Javadoc)
     * @see java.util.Map#get(java.lang.Object)
     */
    public V get(Object key) {
        return mapValue.get((CS) key.toString());
    }
    
    public V safeGet(Object key) {
        return mapValue.get((CS) key.toString());
    }
    
    /* (non-Javadoc)
     * @see java.util.Map#isEmpty()
     */
    public boolean isEmpty() {
        return mapValue.isEmpty();
    }

    /* (non-Javadoc)
     * @see java.util.Map#keySet()
     */
    public Set<CS> keySet() {
        return mapValue.keySet();
    }

    /* (non-Javadoc)
     * @see java.util.Map#put(java.lang.Object, java.lang.Object)
     */
    public V put(CS key, V value) {
        CS keySequence = (CS) key.toString();
        valueOrder.add(keySequence);
        return mapValue.put(keySequence, value);
    }

    /* (non-Javadoc)
     * @see java.util.Map#putAll(java.util.Map)
     */
    public void putAll(Map<? extends CS, ? extends V> m) {
        for (CS k : m.keySet()) {
            valueOrder.add(k);
        }
        mapValue.putAll(m);
    }

    /* (non-Javadoc)
     * @see java.util.Map#remove(java.lang.Object)
     */
    public V remove(Object key) {
        CS keySequence = (CS) key.toString();
        valueOrder.remove(keySequence);
        return mapValue.remove(keySequence);
    }

    /* (non-Javadoc)
     * @see java.util.Map#size()
     */
    public int size() {
        return mapValue.size();
    }

    /* (non-Javadoc)
     * @see java.util.Map#values()
     */
    public Collection<V> values() {
        return mapValue.values();
    }

    public int nestedSize() {
        int c = 0;
        for (V v : values()) {
            c += v.nestedSize();
        }
        return size() + c;
    }

    /* (non-Javadoc)
     * @see com.realitypipe.json.Value#toJSON()
     */
    @Override
    public String toJSON() {
        CS k = null;
        Value v = null;
        Iterator<CS> keyIterator = valueOrder.iterator();
        if (keyIterator.hasNext()) {
            StringBuilder json = new StringBuilder();
            json.append((char) OPEN_OBJECT);
            k = (CS) keyIterator.next();
            v = get(k);
            json.append((char) QUOTATION_MARK).append(k.toString()).append((char) QUOTATION_MARK).append((char) NAME_SEPARATOR).append(v.toJSON());
            while (keyIterator.hasNext()) {
                k = (CS) keyIterator.next();
                v = get(k);
                json.append((char) ',').append((char) QUOTATION_MARK).append(k.toString()).append((char) QUOTATION_MARK).append((char) NAME_SEPARATOR).append(v.toJSON());
            }
            json.append((char) CLOSE_OBJECT);
            return json.toString();
        } else {
            return "{}";
        }
    }
}
